package com.github.fwfurtado.micro9314.posts.shared.infra.rest.clients;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@SpringBootTest
@AutoConfigureStubRunner(ids = "com.github.fwfurtado.micro9314:comments:+:stubs:9090", stubsMode = StubRunnerProperties.StubsMode.LOCAL)
class CommentClientTest {

    @Autowired
    private CommentClient client;

    @Test
    void testClientRestTemplate() {
        var rest = new RestTemplate();

        var uri = UriComponentsBuilder.fromHttpUrl("http://localhost:9090/comments").queryParam("question", 1).build();

        var headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));

        var request = new HttpEntity<Object>(null, headers);

        var response = rest.exchange(uri.toUri(), HttpMethod.GET, request, String.class);

        System.out.println(response);
    }

    @Test
    void testClientFeign() {
        var result = client.findCommentsBy(1L, MediaType.APPLICATION_JSON);

        System.out.println(result);
    }

}