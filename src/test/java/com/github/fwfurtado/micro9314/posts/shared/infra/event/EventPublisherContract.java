package com.github.fwfurtado.micro9314.posts.shared.infra.event;

import com.github.fwfurtado.micro9314.posts.PostsApplication;
import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Post;
import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Question;
import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest(classes = PostsApplication.class)
@AutoConfigureMessageVerifier
class EventPublisherContract {


    @Autowired
    EventPublisher publisher;

    void fire() {
        var question = new Question("Some title", "");
        var post = new Post(question, List.of(new Tag("Java")), LocalDateTime.now());

        ReflectionTestUtils.setField(post, "id", 1L);

        publisher.publish(post);
    }
}