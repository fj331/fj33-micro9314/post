import org.springframework.cloud.contract.spec.Contract

Contract.make {

    label 'create_post'

    input {
        triggeredBy('fire()')
    }

    outputMessage {
        sentTo('post')



        body(
                "id": 1,
                "title": "Some title",
                "userId": 1
        )
    }
}