package com.github.fwfurtado.micro9314.posts.shared.infra.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Post;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;


@Service
public class EventPublisher {

    private final MessageChannel channel;

    public EventPublisher(@Qualifier(PostBinding.BINDING_NAME) MessageChannel binding) {
        this.channel = binding;
    }

    record CreatedPostEvent(
            @JsonProperty Long id,
            @JsonProperty String title,
            @JsonProperty Long userId) {
    }

    public void publish(Post post) {
        var createdPostEvent = new CreatedPostEvent(post.getId(), post.getQuestion().getTitle(), 1L);
        var event = MessageBuilder.withPayload(createdPostEvent).build();

        channel.send(event);
    }

}
