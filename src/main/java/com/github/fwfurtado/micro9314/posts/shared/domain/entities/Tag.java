package com.github.fwfurtado.micro9314.posts.shared.domain.entities;

import javax.persistence.Embeddable;

@Embeddable
public class Tag {

    private String name;

    Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
