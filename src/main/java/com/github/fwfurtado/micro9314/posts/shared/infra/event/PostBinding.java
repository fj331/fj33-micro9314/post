package com.github.fwfurtado.micro9314.posts.shared.infra.event;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

interface PostBinding {
    String BINDING_NAME = "post";

    @Output(BINDING_NAME)
    MessageChannel channel();
}
