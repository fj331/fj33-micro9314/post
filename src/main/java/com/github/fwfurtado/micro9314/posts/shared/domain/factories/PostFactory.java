package com.github.fwfurtado.micro9314.posts.shared.domain.factories;

import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Post;
import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Question;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PostFactory {
    private final TagFactory tagFactory;
    private final Clock clock;

    public PostFactory(TagFactory tagFactory, Clock clock) {
        this.tagFactory = tagFactory;
        this.clock = clock;
    }

    public Post factory(String title, String content, List<String> tags) {
        var question = new Question(title, content);
        var mapped_tags = tags.stream().map(tagFactory::factory).collect(Collectors.toList());

        return new Post(question, mapped_tags, LocalDateTime.now(clock));
    }
}
