package com.github.fwfurtado.micro9314.posts.shared.infra.netflix;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableEurekaClient
class SpringCloudNetflixConfiguration {
}
