package com.github.fwfurtado.micro9314.posts.api.show;

import com.github.fwfurtado.micro9314.posts.api.show.ShowPostController.PostView;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
class ShowPostService {
    private final FindPostRepository repository;
    private final PostViewMapper mapper;

    ShowPostService(FindPostRepository repository, PostViewMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @RateLimiter(name = "comments")
    @Retry(name = "comments", fallbackMethod = "recovery")
    Optional<PostView> showPostBy(Long id) {
        if( id == 2) {
            System.out.println("Throws!!");
            throw new IllegalStateException("Cannot be even");
        }

        if (id == 3) {
            return Optional.empty();
        }

        return repository.findById(id).map(mapper::map);
    }

    Optional<PostView> recovery(Long id, Throwable e) {
        System.out.println("RECOVERY!!!!");
        return Optional.empty();
    }
}
