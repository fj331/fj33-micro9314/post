package com.github.fwfurtado.micro9314.posts.shared.domain.factories;

import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Tag;
import com.github.fwfurtado.micro9314.posts.shared.infra.text.TextNormalizer;
import org.springframework.stereotype.Component;

@Component
public class TagFactory {

    private final TextNormalizer normalizer;

    public TagFactory(TextNormalizer normalizer) {
        this.normalizer = normalizer;
    }

    public Tag factory(String tag) {
        var normalized_tag = normalizer.normalize(tag);

        return new Tag(normalized_tag);
    }
}
