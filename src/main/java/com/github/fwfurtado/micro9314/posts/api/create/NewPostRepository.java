package com.github.fwfurtado.micro9314.posts.api.create;

import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Post;
import org.springframework.data.repository.Repository;

public interface NewPostRepository {
    void save(Post post);
}
