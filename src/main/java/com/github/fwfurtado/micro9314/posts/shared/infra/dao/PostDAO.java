package com.github.fwfurtado.micro9314.posts.shared.infra.dao;

import com.github.fwfurtado.micro9314.posts.api.create.NewPostRepository;
import com.github.fwfurtado.micro9314.posts.api.show.FindPostRepository;
import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Post;
import org.springframework.data.repository.Repository;

interface PostDAO extends Repository<Post, Long>, NewPostRepository, FindPostRepository {
}
