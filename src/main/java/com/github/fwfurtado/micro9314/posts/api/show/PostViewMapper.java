package com.github.fwfurtado.micro9314.posts.api.show;

import com.github.fwfurtado.micro9314.posts.api.show.ShowPostController.PostView;
import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Post;
import com.github.fwfurtado.micro9314.posts.shared.infra.rest.clients.CommentClient;
import org.springframework.http.MediaType;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

@Component
class PostViewMapper {
    private final CommentClient client;
    private final QuestionViewMapper questionViewMapper;

    PostViewMapper(CommentClient client, QuestionViewMapper questionViewMapper) {
        this.client = client;
        this.questionViewMapper = questionViewMapper;
    }

    PostView map(Post post) {
        var comments = client.findCommentsBy(post.getId(), MediaType.APPLICATION_JSON);
        var question = questionViewMapper.map(post.getQuestion(), post.getTags());
        return new PostView(question, comments);
    }

}
