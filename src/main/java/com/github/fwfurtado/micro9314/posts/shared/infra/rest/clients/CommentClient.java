package com.github.fwfurtado.micro9314.posts.shared.infra.rest.clients;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import feign.Headers;
import feign.hystrix.FallbackFactory;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "comments")
public interface CommentClient {

    @GetMapping(value = "/comments")
//    @CircuitBreaker(name = "comments", fallbackMethod = "fallbackFindCommentsBy")
    List<CommentView> findCommentsBy(@RequestParam("question") Long questionId, @RequestHeader(HttpHeaders.ACCEPT) MediaType mediaType);

    default List<CommentView> fallbackFindCommentsBy(Long questionId, Throwable e) {
        return List.of(new CommentView(-1L, "N/A"));
    }


    class CommentView {
        private Long id;
        private String comment;

        private CommentView() {
        }

        public CommentView(Long id, String comment) {
            this.id = id;
            this.comment = comment;
        }

        public Long getId() {
            return id;
        }

        public String getComment() {
            return comment;
        }
    }
//
//    record CommentView(
//            @JsonProperty Long id,
//            @JsonProperty String comment) {
//
//        @JsonCreator
//        static CommentView of(Long id, String comment) {
//            return new CommentView(id, comment);
//        }
//    }
}
