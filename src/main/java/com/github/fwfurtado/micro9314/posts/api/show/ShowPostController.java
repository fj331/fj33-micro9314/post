package com.github.fwfurtado.micro9314.posts.api.show;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.github.fwfurtado.micro9314.posts.api.PostApi;
import com.github.fwfurtado.micro9314.posts.shared.infra.rest.clients.CommentClient.CommentView;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@PostApi
class ShowPostController {
    private final ShowPostService service;

    ShowPostController(ShowPostService service) {
        this.service = service;
    }

    @GetMapping("{id}")
    ResponseEntity<?> showPost(@PathVariable Long id) {
        return service.showPostBy(id).map(ok()::body).orElseGet(notFound()::build);
    }

    record QuestionView(@JsonProperty String title, @JsonProperty String content, @JsonProperty List<String> tags) {
    }

    record PostView(@JsonProperty QuestionView question, @JsonProperty List<CommentView> comments) {
    }

}
