package com.github.fwfurtado.micro9314.posts.api.show;

import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Question;
import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Tag;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.github.fwfurtado.micro9314.posts.api.show.ShowPostController.QuestionView;
import static java.util.stream.Collectors.toList;

@Component
class QuestionViewMapper {
    QuestionView map(Question question, List<Tag> tags) {
        return new QuestionView(question.getTitle(), question.getContent(), tags.stream().map(Tag::getName).collect(toList()));
    }
}
