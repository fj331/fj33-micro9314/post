package com.github.fwfurtado.micro9314.posts.api.create;

import com.github.fwfurtado.micro9314.posts.api.PostApi;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.http.ResponseEntity.created;

@PostApi
class NewPostController {

    private final NewPostService service;

    NewPostController(NewPostService service) {
        this.service = service;
    }

    @PostMapping
    ResponseEntity<?> createANewPost(@RequestBody @Validated NewPost post, UriComponentsBuilder builder) {

        var id = service.createPostBy(post);

        var uri = builder.path("/posts/{id}").build(id);

        return created(uri).build();
    }

    record NewPost(
            String title, String content, String[] tags) {}
}
