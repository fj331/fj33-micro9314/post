package com.github.fwfurtado.micro9314.posts.api.create;

import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Post;
import com.github.fwfurtado.micro9314.posts.shared.domain.factories.PostFactory;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.github.fwfurtado.micro9314.posts.api.create.NewPostController.*;

@Component
class NewPostMapper {

    private final PostFactory factory;

    NewPostMapper(PostFactory factory) {
        this.factory = factory;
    }

    public Post map(NewPost newPost) {
        return factory.factory(newPost.title(), newPost.content(), List.of(newPost.tags()));
    }
}
