package com.github.fwfurtado.micro9314.posts.shared.infra.event;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;

@Configuration
@EnableBinding({Source.class, PostBinding.class})
class BinderConfiguration {

}
