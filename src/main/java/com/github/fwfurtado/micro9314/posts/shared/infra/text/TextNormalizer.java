package com.github.fwfurtado.micro9314.posts.shared.infra.text;

import org.springframework.stereotype.Component;

import java.text.Normalizer;

@Component
public class TextNormalizer {
    private static final String REGEX_TO_REMOVE_ACCENT = "\\p{InCombiningDiacriticalMarks}";

    public String normalize(String tag) {
        return Normalizer
                .normalize(tag, Normalizer.Form.NFD)
                .replace(REGEX_TO_REMOVE_ACCENT, "")
                .toUpperCase();
    }
}
