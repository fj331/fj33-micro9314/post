package com.github.fwfurtado.micro9314.posts.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@RestController
@RequestMapping("posts")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PostApi {
}
