package com.github.fwfurtado.micro9314.posts.api.create;

import com.github.fwfurtado.micro9314.posts.api.create.NewPostController.NewPost;
import com.github.fwfurtado.micro9314.posts.shared.infra.event.EventPublisher;
import org.springframework.stereotype.Service;

@Service
class NewPostService {
    private final NewPostMapper mapper;
    private final NewPostRepository repository;
    private final EventPublisher eventPublisher;

    NewPostService(NewPostMapper mapper, NewPostRepository repository, EventPublisher eventPublisher) {
        this.mapper = mapper;
        this.repository = repository;
        this.eventPublisher = eventPublisher;
    }

    public Long createPostBy(NewPost newPost) {
        var post = mapper.map(newPost);

        repository.save(post);

        eventPublisher.publish(post);

        return post.getId();
    }
}
