package com.github.fwfurtado.micro9314.posts.shared.domain.entities;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private Question question;

    @ElementCollection
    @CollectionTable(name = "post_tags", joinColumns = @JoinColumn(name = "post_id"))
    @AttributeOverrides({
            @AttributeOverride(name = "name", column = @Column(name = "tag"))
    })
    private List<Tag> tags;

    private LocalDateTime time;

    Post() {
    }

    public Post(Question question, List<Tag> tags, LocalDateTime time) {
        this.question = question;
        this.tags = tags;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public Question getQuestion() {
        return question;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public LocalDateTime getTime() {
        return time;
    }
}
