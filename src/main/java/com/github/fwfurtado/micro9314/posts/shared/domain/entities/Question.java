package com.github.fwfurtado.micro9314.posts.shared.domain.entities;

import javax.persistence.Embeddable;

@Embeddable
public class Question {

    private String title;
    private String content;

    Question() {
    }

    public Question(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
