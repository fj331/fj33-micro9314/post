package com.github.fwfurtado.micro9314.posts.api.show;

import com.github.fwfurtado.micro9314.posts.shared.domain.entities.Post;

import java.util.Optional;

public interface FindPostRepository {
    Optional<Post> findById(Long id);
}
